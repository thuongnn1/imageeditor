package com.createchance.imageeditordemo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Work运行器
 *
 * @author createchance
 * @date 21/11/2017
 */

public final class WorkRunner {
    /**
     * Custom background thread pool with priority
     * The number of core threads is: the number of cpu cores + 1
     * The maximum number of threads is: the number of cpu cores x 2 + 1
     * Thread idle keep-alive time is: 60s
     */
    private static ExecutorService mThreadPool = Executors.newFixedThreadPool(
            Runtime.getRuntime().availableProcessors() * 2);

    /**
     * Add a task to the background thread queue to run
     *
     * @param task the task to be executed
     */
    public static void addTaskToBackground(Runnable task) {
        if (task == null) {
            throw new NullPointerException();
        }

        mThreadPool.execute(task);
    }
}
